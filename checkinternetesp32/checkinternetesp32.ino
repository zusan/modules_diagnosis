#include <WiFi.h>
#include <ESP32Ping.h>
 
 
void setup() {
  Serial.begin(115200);
 
  WiFi.begin("Z U S A N 4G", "12345678");
   
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi...");
  }
 
  bool success = Ping.ping("www.google.com", 3);
 
  if(!success){
    Serial.println("Ping failed");
    return;
  }
 
  Serial.println("Ping succesful.");
 
 
}
 
void loop() { }

#include <SoftwareSerial.h>
SoftwareSerial mySerial(3, 4); 
 
void setup()
{
  Serial.begin(9600);  
  mySerial.begin(9600); 
}
 
void loop()
{
  if (mySerial.available()) 
  {
    while (mySerial.available()) 
    {
      char input = mySerial.read(); 
      Serial.print(input); 
      delay(5); 
    }
    Serial.println();
  }
}
